export default class PAGES_URL {
    
    static readonly signIn = "account/signin"
    
    static readonly createAccount = "account/createaccountform"

    static readonly profile = "profile"

}
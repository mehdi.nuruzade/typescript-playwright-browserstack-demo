import {Page,expect} from '@playwright/test';

export default class ProfilePage {

    constructor(private page:Page){};

    async checkPageHeader(){
       await expect(this.page.locator("h1.profile-header-primary")).toHaveText("My Account")
    }    

}
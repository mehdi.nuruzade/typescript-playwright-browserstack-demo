import {Locator, Page,expect} from '@playwright/test';
import ProfilePage from './profile.page';


export default class SignInPage{
    constructor(private page: Page){}
    private profilePage = new ProfilePage(this.page); 


    async signIn(email:string,password:string){
       await this.enterEmailAdress(email);
       await this.enterPassword(password);
       await this.clickSignInButton();
       await this.isSuccessfull();
    }


    async enterEmailAdress(email: string){
        await  this.page.locator('input#EmailAddress')
        .type(email);

    }

    async enterPassword(password:string){
        await  this.page.locator('input#Password')
         .type(password);
     }
   

     async clickSignInButton(){
        await this.page.locator("//button[@type='submit'][text()='Sign in']")
        .click();
     }

     async isSuccessfull(){
        await this.profilePage.checkPageHeader();
     }

     async isUnsuccessfull(alertText: string){
        // const alertText: string = "Sorry, we don't recognize this email address and/or password. Please double check your spelling.";
        const alertLocator: Locator =  await this.page.locator("ul.alert-list>li")
        await expect(alertLocator).toHaveText(alertText);
     }
}
// @ts-check
import {test} from '../util/pom.fixture';
import PAGES_URL  from '../constants/pages.url';
import ENV from '../util/env';


test.describe('Login Page Test Suite',async ()=>{

    test("Login with correct username and password", async ({page,signinPage}) =>{  

        await page.goto(`${ENV.BASE_URL+PAGES_URL.signIn}`);
        await signinPage.enterEmailAdress(ENV.EMAIL);
        await signinPage.enterPassword(ENV.PASSWORD);
        await signinPage.clickSignInButton();
        await signinPage.isSuccessfull();
    })


    test("Login with correct username and wrong password", async ({page,signinPage}) =>{
        await page.goto(`${ENV.BASE_URL+PAGES_URL.signIn}`);
        await signinPage.enterEmailAdress(ENV.EMAIL);
        await signinPage.enterPassword(ENV.WRONG_PASSWORD);
        await signinPage.clickSignInButton();
        await signinPage.isUnsuccessfull("Sorry, we don't recognize this email address and/or password. Please double check your spelling.");
    })


    test("Login with wrong username and correct password", async ({page,signinPage}) =>{
        await page.goto(`${ENV.BASE_URL+PAGES_URL.signIn}`);
        await signinPage.enterEmailAdress(ENV.WRONG_EMAIL);
        await signinPage.enterPassword(ENV.PASSWORD);
        await signinPage.clickSignInButton();
        await signinPage.isUnsuccessfull("Sorry, we don't recognize this email address. Please double check your spelling.");
    })

    test("Login with wrong username and wrong password", async ({page,baseURL,signinPage}) =>{
        await page.goto(`${ENV.BASE_URL+PAGES_URL.signIn}`);
        await signinPage.enterEmailAdress(ENV.WRONG_EMAIL);
        await signinPage.enterPassword(ENV.WRONG_PASSWORD);
        await signinPage.clickSignInButton();
        await signinPage.isUnsuccessfull("Sorry, we don't recognize this email address. Please double check your spelling.");
    })
})

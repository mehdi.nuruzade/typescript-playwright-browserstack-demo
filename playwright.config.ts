// @ts-check
import { PlaywrightTestConfig,devices } from "@playwright/test";
import dotenv from "dotenv";

const { getCdpEndpoint } = require('./browserstack.config.ts')


dotenv.config({
  path:".cloud.env",
  override:true
})



const config : PlaywrightTestConfig = {
  testDir: './tests',
  testMatch: '**/*spec.ts', 
  
  globalSetup: "./global-setup.ts",
  globalTeardown: "./global-teardown.ts",

  /* Maximum time one test can run for. */
  timeout: 90 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000
  },
  /* Run tests in files in parallel */
  fullyParallel: true,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: process.env.CI ? 2 : 0,
  /* Opt out of parallel tests on CI. */
  workers: process.env.CI ? 1 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: 'html',
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0,
    headless: false,
    /* Base URL to use in actions like `await page.goto('/')`. */
    // baseURL: 'http://localhost:3000',

    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: 'on-first-retry',
  },

  /* Configure projects for major browsers */

  projects: [
    {
      name: 'remote chrome@latest:Windows 11',
      use: {
        connectOptions: { wsEndpoint: getCdpEndpoint('chrome@latest:Windows 11','test1') },
      },
    }
    ,
    {
      name: 'remote playwright-webkit@latest:OSX Ventura',
      use: {
        connectOptions: { wsEndpoint: getCdpEndpoint('playwright-webkit@latest:OSX Ventura', 'test2') }
      },
    },
    {
      name: 'remote playwright-firefox:Windows 11',
      use: {
        connectOptions: { wsEndpoint: getCdpEndpoint('playwright-firefox:Windows 11', 'test3') }
      },
    }
  ],

  /* Folder for test artifacts such as screenshots, videos, traces, etc. */
  // outputDir: 'test-results/',

  /* Run your local dev server before starting the tests */
  // webServer: {
  //   command: 'npm run start',
  //   port: 3000,
  // },
};

module.exports = config;

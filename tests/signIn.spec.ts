// @ts-check
import { test } from "../util/pom.fixture";
import PAGES_URL from "../constants/pages.url";
import ENV from "../util/env";
import { cloudExecutorSetResult ,cloudExecutorSetTitle} from "../util/browserstack.executor";

test.describe("Login Page Test Suite", async () => {

  test("Login with correct username and password", async ({
    page,
    signinPage,
  }, testInfo) => {
    try {
      await page.evaluate(()=>{},cloudExecutorSetTitle(testInfo.title))
      await page.evaluate(() => {},cloudExecutorSetTitle(testInfo.title))
      await page.goto(`${ENV.BASE_URL + PAGES_URL.signIn}`);
      await signinPage.enterEmailAdress(ENV.EMAIL);
      await signinPage.enterPassword(ENV.PASSWORD);
      await signinPage.clickSignInButton();
      await signinPage.isSuccessfull();
      await page.evaluate(()=>{},cloudExecutorSetResult('passed',`${testInfo.title} passed.`))
    } catch (e) {
      console.log(e);
      await page.evaluate(()=>{},cloudExecutorSetResult('failed',`${testInfo.title} failed.`))
      throw e;
    }
  });

  test("Login with correct username and wrong password", async ({
    page,
    signinPage,
  }, testInfo) => {
    try {
        await page.evaluate(() => {},cloudExecutorSetTitle(testInfo.title))
      await page.goto(`${ENV.BASE_URL + PAGES_URL.signIn}`);
      await signinPage.enterEmailAdress(ENV.EMAIL);
      await signinPage.enterPassword(ENV.WRONG_PASSWORD);
      await signinPage.clickSignInButton();
      await signinPage.isUnsuccessfull(
        "Sorry, we don't recognize this email address and/or password. Please double check your spelling."
      );
      await page.evaluate(()=>{},cloudExecutorSetResult('passed',`${testInfo.title} passed.`))
    } catch (e) {
      console.log(e);
      await page.evaluate(()=>{},cloudExecutorSetResult('failed',`${testInfo.title} failed.`))
      throw e;
    }
  });

  test("Login with wrong username and correct password", async ({
    page,
    signinPage,
  }, testInfo) => {
    try {
        await page.evaluate(() => {},cloudExecutorSetTitle(testInfo.title));
      await page.goto(`${ENV.BASE_URL + PAGES_URL.signIn}`);
      await signinPage.enterEmailAdress(ENV.WRONG_EMAIL);
      await signinPage.enterPassword(ENV.PASSWORD);
      await signinPage.clickSignInButton();
      await signinPage.isUnsuccessfull(
        "Sorry, we don't recognize this email address. Please double check your spelling."
      );
      await page.evaluate(()=>{},cloudExecutorSetResult('passed',`${testInfo.title} passed.`))
    } catch (e) {
      console.log(e);
      await page.evaluate(()=>{},cloudExecutorSetResult('failed',`${testInfo.title} failed.`))
      throw e
    }
  });

  test("Login with wrong username and wrong password", async ({
    page,
    baseURL,
    signinPage,
  }, testInfo) => {
    try {
        await page.evaluate(() => {},cloudExecutorSetTitle(testInfo.title))
        await page.goto(`${ENV.BASE_URL + PAGES_URL.signIn}`);
      await signinPage.enterEmailAdress(ENV.WRONG_EMAIL);
      await signinPage.enterPassword(ENV.WRONG_PASSWORD);
      await signinPage.clickSignInButton();
      await signinPage.isUnsuccessfull(
        "Sorry, we don't recognize this email address. Please double check your spelling."
      );
      await page.evaluate(()=>{},cloudExecutorSetResult('passed',`${testInfo.title} passed.`))
    } catch (e) {
      console.log(e);
      await page.evaluate(()=>{},cloudExecutorSetResult('failed',`${testInfo.title} failed.`))
      throw e;
    }
  });
});

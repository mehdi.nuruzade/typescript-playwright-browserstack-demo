import ProfilePage from "../pages/profile.page";
import SignInPage from "../pages/signIn.page";


export type Pages = {
    signinPage: SignInPage;
    profilePage: ProfilePage;
}

export {ProfilePage,SignInPage}
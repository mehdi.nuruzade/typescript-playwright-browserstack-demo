
const cloudExecutorSetTitle = (testInfo)=>{
   return `browserstack_executor: ${JSON.stringify({ action: "setSessionName", arguments: { name: testInfo } })}`;
}

const  cloudExecutorSetResult = (status:string,reason:string) => {
    return `browserstack_executor: ${JSON.stringify({action: 'setSessionStatus',arguments: {status: status,reason: reason}})}`

}


export {cloudExecutorSetResult,cloudExecutorSetTitle}

export default class ENV {
    public static BASE_URL = process.env.BASE_URL ? process.env.BASE_URL : "BASE_URL NOT FOUND in the current environmnet"

    public static EMAIL = process.env.EMAIL ?  process.env.EMAIL : "EMAIL NOT FOUND in the current environmnet"
    public static WRONG_EMAIL = process.env.WRONG_EMAIL ? process.env.WRONG_EMAIL : "WRONG_EMAIL NOT FOUND in the current environmnet"

    public static PASSWORD = process.env.PASSWORD ? process.env.PASSWORD : "PASSWORD NOT FOUND in the current environmnet"
    public static WRONG_PASSWORD = process.env.WRONG_PASSWORD ? process.env.WRONG_PASSWORD : "WRONG_PASSWORD NOT FOUND in the current environmnet"
}
import {test as pomtest } from '@playwright/test';
import {
     Pages , ProfilePage ,SignInPage
    } from '../types/pages.type';

    

const testPages = pomtest.extend<Pages>({
    //page instances
    signinPage: async ( {page} ,use ) =>{
        await use(new SignInPage(page));
    },
    profilePage: async ( {page} ,use ) =>{
        await use(new ProfilePage(page));
    },
    

});

export const test = testPages;
export const expect = testPages.expect;
